﻿// ***********************************************************************
// Assembly         : OmsaIPFix.Net2
// Author           : andreas
// Created          : 09-21-2013
//
// Last Modified By : andreas
// Last Modified On : 09-21-2013
// ***********************************************************************
// <copyright file="Program.cs" company="Andreas Pietzsch">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary>This Application is intended to fix Dell OMSA behavior to Search for own IP via Netbios Request</summary>
// ***********************************************************************

namespace OmsaIPFix.Net2
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using System.ServiceProcess;
    using System.Xml;

    using Microsoft.Win32;

    /// <summary>
    /// Application Main Class
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The welcome Warning
        /// </summary>
        private const string WelcomeWarning =
                                          "***************************************************************************\r\n"
                                        + "**        Attention: This is not a Official Dell, Inc application        **\r\n"
                                        + "** Use at your own risk. If any question ask the Agent who gave it to you**\r\n"
                                        + "**         https://bitbucket.org/andreas_pietzsch/omsaipfix.net2         **\r\n"
                                        + "***************************************************************************\r\n";

        /// <summary>
        /// The OMSA Registry Key string
        /// </summary>
        private const string OmsaRegKeyStr =
            @"SOFTWARE\Dell Computer Corporation\OpenManage\Applications\SystemsManagement";

        /// <summary>
        /// The configuration file path
        /// </summary>
        private static string configFilePath = string.Empty;

        /// <summary>
        /// The OMSA version
        /// </summary>
        private static string omsaVersion = string.Empty;

        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            GetOmsaInfoFromRegistry();
            if (args.Length > 0)
            {
                switch (args[0])
                {
                    case "/?":
                        {
                            ShowAvailableParameters();
                            break;
                        }

                    case "/hostname":
                        {
                            // Write Hotsname to Config File
                            Console.WriteLine("Write Hostname: {0}", Dns.GetHostName());
                            WriteValueToConfigFile(configFilePath, Dns.GetHostName());
                            break;
                        }

                    case "/manual":
                        {
                            if (args.Length < 2)
                            {
                                Console.WriteLine("Error: Argument missing");
                                return;
                            }

                            Console.WriteLine("Write Manual Value: {0}", args[1]);
                            WriteValueToConfigFile(configFilePath, args[1]);
                            break;
                        }

                    case "/hostip":
                        {
                            if (args.Length < 2)
                            {
                                Console.WriteLine("Error: Argument missing");
                                return;
                            }

                            List<string> hostIpList = GetHostIp();
                            string s = GetSelectedIp(hostIpList, args[1]);
                            if (string.IsNullOrEmpty(s))
                            {
                                Console.WriteLine("Error: Invalid IP Index");
                                return;
                            }

                            Console.WriteLine("Write Host IP: {0}", s);
                            WriteValueToConfigFile(configFilePath, s);
                            break;
                        }

                    case "/reset":
                        {
                            Console.WriteLine("Reset to Default - *");
                            WriteValueToConfigFile(configFilePath, "*");
                            break;
                        }

                    default:
                        {
                            Console.WriteLine("Error: unknown parameter");
                            break;
                        }
                }
            }
            else
            {
                Console.WriteLine(WelcomeWarning);

                if (string.IsNullOrEmpty(configFilePath))
                {
                    // No Config file found - Exit
                    Console.WriteLine("OMSA Config File not found.");
                    Console.WriteLine("Press a Key to exit...");
                    Console.Read();
                    return;
                }

                Console.WriteLine("Detected OMSA Version: {0}", omsaVersion);
                Console.WriteLine(string.Empty);

                Console.WriteLine("Detected IP Addresses and Hostname:");
                List<string> selectionList = GetHostIp();

                // List available IP Addresses for Customer to choose
                for (int i = 0; i < selectionList.Count; i++)
                {
                    Console.WriteLine("[{0}]: {1}", i, selectionList[i]);
                }

                // Add Hostname and Abort Option
                Console.WriteLine("[H]: Host Name: {0}", Dns.GetHostName());
                Console.WriteLine("[R]: Reset to default");
                Console.WriteLine("[X]: Abort");

                // Get Customer selection as String
                var s = GetCustomerSelection(selectionList);

                if (string.IsNullOrEmpty(s))
                {
                    // User aborted
                    Console.WriteLine("Abort");
                    return;
                }

                Console.WriteLine("You selected: {0}", s);

                WriteValueToConfigFile(configFilePath, s);
            }

            // Restart Service
            Console.WriteLine("Restarting Services");
            RestartService("Server Administrator", 5000);
        }

        /// <summary>
        /// Gets the Host IPs.
        /// </summary>
        /// <returns><c>List<c>String</c>></c> of all Host IPs including 127.0.0.1</returns>
        private static List<string> GetHostIp()
        {
            var hostIp = new List<string> { "127.0.0.1" };
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    hostIp.Add(ip.ToString());
                }
            }

            return hostIp;
        }

        /// <summary>
        /// Gets the OMSA information from the Registry.
        /// </summary>
        private static void GetOmsaInfoFromRegistry()
        {
            // 1st: Read Omsa Install dir from Registry
            RegistryKey regKey = Registry.LocalMachine;
            RegistryKey subKey = regKey.OpenSubKey(OmsaRegKeyStr);

            // Verify Registry Key is available
            if (subKey == null)
            {
                // if not - exit
                return;
            }

            try
            {
                // 2nd: Combine Install Dir with Config File Path
                configFilePath = (string)subKey.GetValue("InstallPath");
                configFilePath = Path.Combine(configFilePath, @"apache-tomcat\conf\server.xml");

                // 3rd: Get Omsa Version String
                omsaVersion = (string)subKey.GetValue("Version");
            }
            catch
            {
                // we don't care about exception - assume value doest exists
                configFilePath = string.Empty;

                // omsaVersion = string.Empty;
            }
        }

        /// <summary>
        /// Shows available parameters - /?.
        /// </summary>
        private static void ShowAvailableParameters()
        {
            Console.WriteLine(WelcomeWarning);
            Console.WriteLine("Avalibale Parameters.");
            Console.WriteLine();
            Console.WriteLine("<no parameter>\t- application ask what to enter");
            Console.WriteLine("/hostname\t- application Detects Hostname and add them to the config file");
            Console.WriteLine("/hostip <Index>\t- app detects Host Ip Address and add IP by <index> to conf file");
            Console.WriteLine("\t\t- 0 is always 127.0.0.1");
            Console.WriteLine("/manual <Value>\t- IP Address or Hostname is provided via Parameter");
            Console.WriteLine("/reset\t- config file is reset to default - *");
            Console.WriteLine("/?\t- Show this Parameter List");
        }

        /// <summary>
        /// Writes the value to the configuration file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="value">The value.</param>
        private static void WriteValueToConfigFile(string fileName, string value)
        {
            if (string.IsNullOrEmpty(configFilePath))
            {
                // No Config file found - Exit
                Console.WriteLine("Config File not found. Exit");
                Console.WriteLine("Press Enter to continue...");
                Console.Read();
                return;
            }

            // Create a copy of the Config File
            File.Copy(fileName, string.Format("{0}.old", fileName), true);

            // Load Config File
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(fileName);

            // Find Node
            XmlNode node = xmlDocument.SelectSingleNode("/Server/Service/Connector/@address");
            if (node == null)
            {
                // Node not found > exit
                return;
            }

            // Change value
            node.Value = value;

            // Save file
            xmlDocument.Save(fileName);
        }

        /// <summary>
        /// Gets the selected IP.
        /// </summary>
        /// <param name="hostIpList">The host IP list.</param>
        /// <param name="index">List Index</param>
        /// <returns>The selected IP</returns>
        private static string GetSelectedIp(IList<string> hostIpList, string index)
        {
            int selection;

            // Check if selection was a Integer and Within IP List Range
            if (int.TryParse(index, out selection) && (selection < hostIpList.Count))
            {
                // Convert was succesfull - return selected IP
                return hostIpList[selection];
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the customer selection.
        /// </summary>
        /// <param name="hostIpList">The Host IP list.</param>
        /// <returns>Get the Customer selected IP/Hostname - Empty if aborted</returns>
        private static string GetCustomerSelection(IList<string> hostIpList)
        {
            while (true)
            {
                // Ask for Customer selection
                Console.Write("Please choose: ");
                var line = Console.ReadLine();
                if (line == null)
                {
                    continue;
                }

                var s = line.ToLower();

                // Check if user selected Abort
                if (s == "a")
                {
                    // Customer selected Abort - return empty
                    return string.Empty;
                }

                if (s == "h")
                {
                    // Customer selected Host Name
                    return Dns.GetHostName();
                }

                // reset to default
                if (s == "r")
                {
                    return "*";
                }

                var value = GetSelectedIp(hostIpList, s);

                if (string.IsNullOrEmpty(value))
                {
                    // customer made Invalid Selection
                    Console.WriteLine();
                    Console.WriteLine("Invalid Selection");
                }
                else
                {
                    return value;
                }
            }
        }

        /// <summary>
        /// Restarts the service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="timeoutMilliseconds">The timeout in milliseconds.</param>
        private static void RestartService(string serviceName, int timeoutMilliseconds)
        {
            var service = new ServiceController(serviceName);
            try
            {
                var millisec1 = Environment.TickCount;
                var timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

                // count the rest of the timeout
                var millisec2 = Environment.TickCount;
                timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch
            {
                Console.WriteLine("Unable to restart Service {0}", serviceName);
            }
        }
    }
}
